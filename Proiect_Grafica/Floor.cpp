#include "Floor.h"

Floor::Floor(unsigned int planeVAO):
	m_planeVAO(planeVAO)
{
}

void Floor::renderFloor()
{

	float dimension = 400.f;

	if (m_planeVAO == 0) {
		// set up vertex data (and buffer(s)) and configure vertex attributes
		float planeVertices[] = {
			// positions            // normals // texcoords
			dimension, -0.5f,  dimension, 0.0f, 1.0f, 0.0f, dimension, 0.0f,
			-dimension, -0.5f,  dimension, 0.0f, 1.0f, 0.0f,   0.0f, 0.0f,
			-dimension, -0.5f, -dimension,  0.0f, 1.0f, 0.0f, 0.0f, dimension,

			dimension, -0.5f,  dimension, 0.0f, 1.0f, 0.0f,  dimension, 0.0f,
			-dimension, -0.5f, -dimension,  0.0f, 1.0f, 0.0f, 0.0f, dimension,
			dimension, -0.5f, -dimension,  0.0f, 1.0f, 0.0f, dimension, dimension
		};
		// plane VAO
		glGenVertexArrays(1, &m_planeVAO);
		glGenBuffers(1, &m_planeVBO);
		glBindVertexArray(m_planeVAO);
		glBindBuffer(GL_ARRAY_BUFFER, m_planeVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertices), planeVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
		glBindVertexArray(0);
	}

	glBindVertexArray(m_planeVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
}
