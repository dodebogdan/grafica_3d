#pragma once

#include "Shader.h"

class Floor
{
public:
	Floor(unsigned int planeVAO = 0);
	void renderFloor();

private:
	unsigned int m_planeVAO;
	unsigned int m_planeVBO;

};

