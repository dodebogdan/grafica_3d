#include <stdlib.h> // necesare pentru citirea shaderStencilTesting-elor
#include <stdio.h>
#include <math.h> 

#include "Camera.h"
#include "Floor.h"
#include "Model.h"
#include "Skybox.h"
#include "Settings.h"


#define NUM_PROGRAMS 5


Camera* pCamera = nullptr;

unsigned int CreateTexture(const std::string& strTexturePath)
{
	unsigned int textureId = -1;

	// load image, create texture and generate mipmaps
	int width, height, nrChannels;
	stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
	unsigned char* data = stbi_load(strTexturePath.c_str(), &width, &height, &nrChannels, 0);
	if (data) {
		GLenum format;
		if (nrChannels == 1)
			format = GL_RED;
		else if (nrChannels == 3)
			format = GL_RGB;
		else if (nrChannels == 4)
			format = GL_RGBA;

		glGenTextures(1, &textureId);
		glBindTexture(GL_TEXTURE_2D, textureId);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		// set the texture wrapping parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		// set texture filtering parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	else {
		std::cout << "Failed to load texture: " << strTexturePath << std::endl;
	}
	stbi_image_free(data);

	return textureId;
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void processInput(GLFWwindow* window);

void renderScene(const Shader& shader);

// timing
double deltaTime = 0.0f;    // time between current frame and last frame
double lastFrame = 0.0f;

int lightStart = 0;

Floor* f = new Floor();

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_L && action == GLFW_PRESS)
	{
		lightStart = 1;
	}
	if (key == GLFW_KEY_S && action == GLFW_PRESS)
	{
		lightStart = 0;
	}

}
void glcppShaderSource(GLuint shader, std::string const& shader_string)
{
	GLchar const* shader_source = shader_string.c_str();
	GLint const shader_length = shader_string.size();

	glShaderSource(shader, 1, &shader_source, &shader_length);
}
std::string get_file_contents(const char* filename)
{
	std::ifstream ifs(filename);
	std::string content((std::istreambuf_iterator<char>(ifs)),
		(std::istreambuf_iterator<char>()));
	return content;
}

int main(int argc, char** argv)
{
	std::string strFullExeFileName = argv[0];
	std::string strExePath;
	const size_t last_slash_idx = strFullExeFileName.rfind('\\');
	if (std::string::npos != last_slash_idx) {
		strExePath = strFullExeFileName.substr(0, last_slash_idx);
	}

	// glfw: initialize and configure
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// glfw window creation
	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Lord Of the Rings Orc phantom Training scene", NULL, NULL);
	if (window == NULL) {
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);
	glfwSetKeyCallback(window, key_callback);

	// tell GLFW to capture our mouse
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	glewInit();

	// Create camera
	pCamera = new Camera(SCR_WIDTH, SCR_HEIGHT, glm::vec3(0.0, 1.0, 3.0));

	// configure global opengl state
	// -----------------------------
	glEnable(GL_DEPTH_TEST);

	// build and compile shaders
	// -------------------------
	Shader shadowMappingShader("ShadowMapping.vs", "ShadowMapping.fs");
	Shader shadowMappingDepthShader("ShadowMappingDepth.vs", "ShadowMappingDepth.fs");

	// load textures
	// -------------
	unsigned int floorTexture = CreateTexture(strExePath + "\\rock.png");
	unsigned int knightTexture = CreateTexture(strExePath + "\\armor.jpg");
	unsigned int towerTexture = CreateTexture(strExePath + "\\Diffuse.jpeg");
	unsigned int fountainTexture = CreateTexture(strExePath + "\\hut0292.png");
	// load cube
	// -------------
	//unsigned int cubeTexture = CreateTexture(strExePath + "\\golum.png");

	//shader
	Shader shaderBlending("Blending.vs", "Blending.fs");
	shaderBlending.SetInt("texture1", 0);
	// configure depth map FBO
	// -----------------------
	const unsigned int SHADOW_WIDTH = 4096, SHADOW_HEIGHT = 4096;
	unsigned int depthMapFBO;
	glGenFramebuffers(1, &depthMapFBO);
	// create depth texture
	unsigned int depthMap;
	glGenTextures(1, &depthMap);
	glBindTexture(GL_TEXTURE_2D, depthMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	float borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
	// attach depth texture as FBO's depth buffer
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);


	// shader configuration
	// --------------------
	shadowMappingShader.Use();
	shadowMappingShader.SetInt("diffuseTexture", 0);
	shadowMappingShader.SetInt("shadowMap", 1);

	// lighting info
	// -------------
	glm::vec3 lightPos(-2.0f, 4.0f, -1.0f);

	glEnable(GL_CULL_FACE);

	ModelShader modelShader("modelLoading.vs", "modelLoading.fs");
	Model Tree("../Proiect_Grafica/Tree/10460_Yellow_Poplar_Tree_v1_L3.obj");
	Model Knight("../Proiect_Grafica/knight/knight.obj");
    Model Well("../Proiect_Grafica/well/3d we.obj");
    Model Tower("../Proiect_Grafica/tower/tower.obj");
    
	Skybox* s = new Skybox();
	while (!glfwWindowShouldClose(window))
	{
		// per-frame time logic
		// --------------------
		float currentFrame = (float)glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		// input
		// -----
		processInput(window);

		// render
		// ------
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

		shaderBlending.Use();
		glm::mat4 projection = pCamera->GetProjectionMatrix();
		glm::mat4 view = pCamera->GetViewMatrix();
		shaderBlending.SetMat4("projection", projection);
		shaderBlending.SetMat4("view", view);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		if (lightStart != 0) {

			lightPos.x = cos(currentFrame);
			lightPos.z = sin(currentFrame);
		}
		else {
			lightPos.x = -2.0f;
			lightPos.y = 4.0f;
			lightPos.z = -1.0f;
		}

		// 1. render depth of scene to texture (from ight's perspective)
		glm::mat4 lightProjection, lightView;
		glm::mat4 lightSpaceMatrix;
		float near_plane = 1.0f, far_plane = 7.5f;
		lightProjection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, near_plane, far_plane);
		lightView = glm::lookAt(lightPos, glm::vec3(0.0f), glm::vec3(0.0, 1.0, 0.0));
		lightSpaceMatrix = lightProjection * lightView;

		// render scene from light's point of view
		shadowMappingDepthShader.Use();
		shadowMappingDepthShader.SetMat4("lightSpaceMatrix", lightSpaceMatrix);

		glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
		glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
		glClear(GL_DEPTH_BUFFER_BIT);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, floorTexture);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);
		renderScene(shadowMappingDepthShader);
		glCullFace(GL_BACK);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		// reset viewport
		glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// 2. render scene as normal using the generated depth/shadow map 
		glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		shadowMappingShader.Use();
		shadowMappingShader.SetMat4("projection", projection);
		shadowMappingShader.SetMat4("view", view);
		// set light uniforms
		shadowMappingShader.SetVec3("viewPos", pCamera->GetPosition());
		shadowMappingShader.SetVec3("lightPos", lightPos);
		shadowMappingShader.SetMat4("lightSpaceMatrix", lightSpaceMatrix);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, floorTexture);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, depthMap);
		glDisable(GL_CULL_FACE);

		renderScene(shadowMappingShader);

		//model
		modelShader.use();
		glm::mat4 projectionM = pCamera->GetProjectionMatrix();
		glm::mat4 viewM = pCamera->GetViewMatrix();
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "projection"), 1, GL_FALSE, glm::value_ptr(projectionM));
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "view"), 1, GL_FALSE, glm::value_ptr(viewM));

		glm::mat4 modelM;

		/*modelM = glm::mat4();
		modelM = glm::translate(modelM, glm::vec3(1.0f, -0.5f, 1.0f));
		modelM = glm::scale(modelM, glm::vec3(0.001f, 0.001f, 0.001f));
		modelM = glm::rotate(modelM, glm::radians(270.f), glm::vec3(1.f, 0.f, 0.f));
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
		Tree.Draw(modelShader);*/


		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, knightTexture);
		modelM = glm::mat4();
		modelM = glm::translate(modelM, glm::vec3(1.0f, -0.5f, 1.0f));
		modelM = glm::scale(modelM, glm::vec3(20.0f, 20.0f, 20.0f));
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
		modelShader.setMat4("model", modelM);
		Knight.Draw(modelShader);


		float posX = 20.0f;
		float posY = 20.0f;
		for (int i = 0; i < 10; i++)
		{
			modelM = glm::mat4();
			modelM = glm::translate(modelM, glm::vec3(posX, 0.0f, 1.0f));
			modelM = glm::scale(modelM, glm::vec3(5.0f, 5.0f, 5.0f));
			glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
			Knight.Draw(modelShader);
			posX += 20;
		}
		
		float posZ = -20.0f;
		for (int j = 0; j < 3; j++)
		{
			posX = -20.0f;
			for (int i = 0; i < 10; i++)
			{
				modelM = glm::mat4();
				modelM = glm::translate(modelM, glm::vec3(posX, 0.0f, posZ));
				modelM = glm::scale(modelM, glm::vec3(5.0f, 5.0f, 5.0f));
				modelM = glm::rotate(modelM, glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.f));
				glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
				Knight.Draw(modelShader);

				posX += 20;
			}
			posZ -= 20;
		}

		for (int j = 0; j < 3; j++)
		{
			posX = 20.0f;
			for (int i = 0; i < 10; i++)
			{
				modelM = glm::mat4();
				modelM = glm::translate(modelM, glm::vec3(posX, 0.0f, posZ));
				modelM = glm::scale(modelM, glm::vec3(5.0f, 5.0f, 5.0f));
				glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
				Knight.Draw(modelShader);

				posX += 20;
			}
			posZ -= 20;
		}

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, towerTexture);
		modelM = glm::mat4();
		modelM = glm::translate(modelM, glm::vec3(-200.0f, -0.5f, 200.0f));
		modelM = glm::scale(modelM, glm::vec3(20.0f, 20.0f, 20.0f));
		//modelM = glm::rotate(modelM, glm::radians(270.f), glm::vec3(1.f, 0.f, 0.f));
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
		Tower.Draw(modelShader);

		modelM = glm::mat4();
		modelM = glm::translate(modelM, glm::vec3(200.0f, -0.5f, 200.0f));
		modelM = glm::scale(modelM, glm::vec3(20.0f, 20.0f, 20.0f));
		//modelM = glm::rotate(modelM, glm::radians(270.f), glm::vec3(1.f, 0.f, 0.f));
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
		Tower.Draw(modelShader);

		modelM = glm::mat4();
		modelM = glm::translate(modelM, glm::vec3(200.0f, -0.5f,-200.0f));
		modelM = glm::scale(modelM, glm::vec3(20.0f, 20.0f, 20.0f));
		//modelM = glm::rotate(modelM, glm::radians(270.f), glm::vec3(1.f, 0.f, 0.f));
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
		Tower.Draw(modelShader);

		modelM = glm::mat4();
		modelM = glm::translate(modelM, glm::vec3(-200.0f, -0.5f, -200.0f));
		modelM = glm::scale(modelM, glm::vec3(20.0f, 20.0f, 20.0f));
		//modelM = glm::rotate(modelM, glm::radians(270.f), glm::vec3(1.f, 0.f, 0.f));
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
		Tower.Draw(modelShader);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, fountainTexture);

		modelM = glm::mat4();
		modelM = glm::translate(modelM, glm::vec3(50.0f, -0.5f, 12.0f));
		modelM = glm::scale(modelM, glm::vec3(0.05f, 0.05f, 0.05f));
		//modelM = glm::rotate(modelM, glm::radians(270.f), glm::vec3(1.f, 0.f, 0.f));
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
		Well.Draw(modelShader);

		modelM = glm::mat4();
		modelM = glm::translate(modelM, glm::vec3(10.0f, -0.5f, 1.0f));
		modelM = glm::scale(modelM, glm::vec3(0.005f, 0.005f, 0.005f));
		modelM = glm::rotate(modelM, glm::radians(270.f), glm::vec3(1.f, 0.f, 0.f));
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
		Tree.Draw(modelShader);
	

		modelM = glm::mat4();
		modelM = glm::translate(modelM, glm::vec3(15.0f, -0.5f, 13.0f));
		modelM = glm::scale(modelM, glm::vec3(0.005f, 0.005f, 0.005f));
		modelM = glm::rotate(modelM, glm::radians(270.f), glm::vec3(1.f, 0.f, 0.f));
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
		Tree.Draw(modelShader);

		modelM = glm::mat4();
		modelM = glm::translate(modelM, glm::vec3(-10.0f, -0.5f, -13.0f));
		modelM = glm::scale(modelM, glm::vec3(0.005f, 0.005f, 0.005f));
		modelM = glm::rotate(modelM, glm::radians(270.f), glm::vec3(1.f, 0.f, 0.f));
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
		Tree.Draw(modelShader);

		modelM = glm::mat4();
		modelM = glm::translate(modelM, glm::vec3(-15.0f, -0.5f, 15.0f));
		modelM = glm::scale(modelM, glm::vec3(0.005f, 0.005f, 0.005f));
		modelM = glm::rotate(modelM, glm::radians(270.f), glm::vec3(1.f, 0.f, 0.f));
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
		Tree.Draw(modelShader);
		
		modelM = glm::mat4();
		modelM = glm::translate(modelM, glm::vec3(10.0f, -0.5f, 1.0f));
		modelM = glm::scale(modelM, glm::vec3(0.005f, 0.005f, 0.005f));
		modelM = glm::rotate(modelM, glm::radians(270.f), glm::vec3(1.f, 0.f, 0.f));
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
		Tree.Draw(modelShader);

		modelM = glm::mat4();
		modelM = glm::translate(modelM, glm::vec3(15.0f, -0.5f, 13.0f));
		modelM = glm::scale(modelM, glm::vec3(0.005f, 0.005f, 0.005f));
		modelM = glm::rotate(modelM, glm::radians(270.f), glm::vec3(1.f, 0.f, 0.f));
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
		Tree.Draw(modelShader);

		modelM = glm::mat4();
		modelM = glm::translate(modelM, glm::vec3(5.0f, -0.5f, -13.0f));
		modelM = glm::scale(modelM, glm::vec3(0.005f, 0.005f, 0.005f));
		modelM = glm::rotate(modelM, glm::radians(270.f), glm::vec3(1.f, 0.f, 0.f));
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
		Tree.Draw(modelShader);

		modelM = glm::mat4();
		modelM = glm::translate(modelM, glm::vec3(-15.0f, -0.5f, 7.0f));
		modelM = glm::scale(modelM, glm::vec3(0.005f, 0.005f, 0.005f));
		modelM = glm::rotate(modelM, glm::radians(270.f), glm::vec3(1.f, 0.f, 0.f));
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
		Tree.Draw(modelShader);	
		
		modelM = glm::mat4();
		modelM = glm::translate(modelM, glm::vec3(16.0f, -0.5f, 31.0f));
		modelM = glm::scale(modelM, glm::vec3(0.005f, 0.005f, 0.005f));
		modelM = glm::rotate(modelM, glm::radians(270.f), glm::vec3(1.f, 0.f, 0.f));
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
		Tree.Draw(modelShader);

		modelM = glm::mat4();
		modelM = glm::translate(modelM, glm::vec3(11.0f, -0.5f, 5.0f));
		modelM = glm::scale(modelM, glm::vec3(0.005f, 0.005f, 0.005f));
		modelM = glm::rotate(modelM, glm::radians(270.f), glm::vec3(1.f, 0.f, 0.f));
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
		Tree.Draw(modelShader);

		modelM = glm::mat4();
		modelM = glm::translate(modelM, glm::vec3(-13.0f, -0.5f, -6.0f));
		modelM = glm::scale(modelM, glm::vec3(0.005f, 0.005f, 0.005f));
		modelM = glm::rotate(modelM, glm::radians(270.f), glm::vec3(1.f, 0.f, 0.f));
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
		Tree.Draw(modelShader);

		modelM = glm::mat4();
		modelM = glm::translate(modelM, glm::vec3(-7.0f, -0.5f, 12.0f));
		modelM = glm::scale(modelM, glm::vec3(0.005f, 0.005f, 0.005f));
		modelM = glm::rotate(modelM, glm::radians(270.f), glm::vec3(1.f, 0.f, 0.f));
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
		Tree.Draw(modelShader);
		
		modelM = glm::mat4();
		modelM = glm::translate(modelM, glm::vec3(5.0f, -0.5f, 15.0f));
		modelM = glm::scale(modelM, glm::vec3(0.005f, 0.005f, 0.005f));
		modelM = glm::rotate(modelM, glm::radians(270.f), glm::vec3(1.f, 0.f, 0.f));
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
		Tree.Draw(modelShader);

		modelM = glm::mat4();
		modelM = glm::translate(modelM, glm::vec3(7.0f, -0.5f, 12.0f));
		modelM = glm::scale(modelM, glm::vec3(0.005f, 0.005f, 0.005f));
		modelM = glm::rotate(modelM, glm::radians(270.f), glm::vec3(1.f, 0.f, 0.f));
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
		Tree.Draw(modelShader);

		modelM = glm::mat4();
		modelM = glm::translate(modelM, glm::vec3(21.0f, -0.5f, -1.0f));
		modelM = glm::scale(modelM, glm::vec3(0.005f, 0.005f, 0.005f));
		modelM = glm::rotate(modelM, glm::radians(270.f), glm::vec3(1.f, 0.f, 0.f));
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
		Tree.Draw(modelShader);

		modelM = glm::mat4();
		modelM = glm::translate(modelM, glm::vec3(-15.0f, -0.5f,10.0f));
		modelM = glm::scale(modelM, glm::vec3(0.005f, 0.005f, 0.005f));
		modelM = glm::rotate(modelM, glm::radians(270.f), glm::vec3(1.f, 0.f, 0.f));
		glUniformMatrix4fv(glGetUniformLocation(modelShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(modelM));
		Tree.Draw(modelShader);

		enum {
			PROGRAM_SIMPLE_TERRAIN,
			PROGRAM_SIMPLE_LIGHTBEAM,
			PROGRAM_SIMPLE_LIGHTBALL,
			PROGRAM_COMPLEX_NORMALMAP_LAVAGLOW,
			PROGRAM_COMPLEX_NORMALMAP
		};

		projection = glm::perspective(glm::radians(45.0f), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
		view = glm::mat4(glm::mat3(pCamera->GetViewMatrix()));
		s->Draw(view, projection);
		//obj->UpdateThenDraw(*pCamera);

		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	// optional: de-allocate all resources once they've outlived their purpose:
	//delete f;
	delete pCamera;
	//delete t;

	glfwTerminate();
	return 0;
}

// renders the 3D scene
// --------------------
void renderScene(const Shader& shader)
{
	// floor
	glm::mat4 model;
	shader.SetMat4("model", model);
	f->renderFloor();
}


// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		pCamera->ProcessKeyboard(FORWARD, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		pCamera->ProcessKeyboard(BACKWARD, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		pCamera->ProcessKeyboard(LEFT, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		pCamera->ProcessKeyboard(RIGHT, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_PAGE_UP) == GLFW_PRESS)
		pCamera->ProcessKeyboard(UP, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_PAGE_DOWN) == GLFW_PRESS)
		pCamera->ProcessKeyboard(DOWN, (float)deltaTime);

	if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
		int width, height;
		glfwGetWindowSize(window, &width, &height);
		pCamera->Reset(width, height);

	}
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and 
	// height will be significantly larger than specified on retina displays.
	pCamera->Reshape(width, height);
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	pCamera->MouseControl((float)xpos, (float)ypos);
}

void scroll_callback(GLFWwindow* window, double xoffset, double yOffset)
{
	pCamera->ProcessMouseScroll((float)yOffset);
}
