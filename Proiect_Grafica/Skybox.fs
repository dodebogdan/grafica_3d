#version 330 core

out vec4 FragColor;

in vec3 TexCoords;

uniform samplerCube skybox;

void main()
{    
    vec4 frag=texture(skybox, TexCoords);
    FragColor=vec4(frag.xyz,1.0);
}