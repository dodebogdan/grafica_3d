#pragma once
#include"Shader.h"
#include<vector>
class Skybox
{
public:
	Skybox();
	~Skybox();

	void Draw(const glm::mat4& view, const glm::mat4& projection);

private:
	unsigned int cubemapTexture;

	GLuint VAO = 0;
	GLuint VBO = 0;

	Shader* shader;

	GLuint loadCubemap(std::vector<std::string> faces);
};


